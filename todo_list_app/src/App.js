import MuiTest from "./Components/MuiTest"
import StateContainer from "./Components/StateContainer";

function App() {
  return (
    <div>
      <MuiTest/>
      <StateContainer/>
    </div>
  )
}

export default App;
