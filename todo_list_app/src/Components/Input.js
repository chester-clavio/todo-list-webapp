import { Button, OutlinedInput } from "@mui/material"

export default function Input ({stateChanger,...rest}){

    return(
        <OutlinedInput onChange={(e)=>stateChanger(e.target.value)}/>
    )
}