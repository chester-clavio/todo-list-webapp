
import { useState } from "react";
import Display from "./Display";
import Input from "./Input";


export default function StateContainer(){
    const [input,setInput]=useState('')
    return(
        <div>
            <Display input={input}/>
            <Input stateChanger={setInput}/>
        </div>
    )
}
